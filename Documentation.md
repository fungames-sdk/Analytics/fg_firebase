# Firebase

## Integration Steps

1) **"Install"** or **"Upload"** FG Firebase plugin from the FunGames Integration Manager.

2) Follow the instructions in the **"Install External Plugin"** section to import FirebaseAnalytics SDK.

3) Click on the **"Prefabs and Settings"** button in the FunGames Integration Manager to fill up your scene with required components and create the Settings asset.

4) To finish your integration, follow the **Account and Settings** section.

## Install External Plugin

To integrate Firebase Analytics and use it through the FunGames SDK you first need to install the <a href="https://firebase.google.com/docs/unity/setup" target="_blank">Firebase Unity SDK</a> into your project.

**If using different packages for different Firebase services (ex: Analytics, RemoteConfig, Cloud Messaging, etc.), make sure they all corresponds to the same version of the sdk.**

## Account and Settings

First you will have to create your application on the <a href="https://console.firebase.google.com/" target="_blank">Firebase console</a>. Please follow the steps indicated during the creation, including enabling Google Analytics. Once the application is created, please click on the Unity logo and follow the steps to create the applications. Please make sure to download the configuration files that you will need to put in your assets folder.

## Custom Event Integration

To integrate custom Firebase events requested by UA, please install the following module:

- <a href="fg_firebase_custom_tracker.html" target="_blank">Firebase Custom Tracker</a>

# TROUBLESHOOTING

## Check And Fix Dependencies

When initializing Firebase in the SDK, its often required to make sure the necessary dependencies for Firebase are present on the system, and in the necessary state. This is done through the _FirebaseApp.CheckAndFixDependenciesAsync()_ function.

This function should be called **only once** in code, to prevent from app critical errors or crashes.

In FunGamesSDK we provide a wrapper for this (see _FGFirebaseDependencyChecker.cs_) that you can use as a simple singleton in code, to make sure the Firebase check is made only once. You can then retrieve the completion state by subcscribing to the _FGFirebaseDependencyChecker.OnDependencyResolved_ callback.